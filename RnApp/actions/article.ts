import * as types from './action-types';
import {API} from './action-types';
import {Post} from '../models/Post';

export function fetchArticleDetails() {
  return apiAction({
    url: 'https://api.npoint.io/3bb76d4bee766758ad6a',
    onSuccess: setArticleDetails,
    onFailure: () => console.log('Error occured loading articles'),
    label: types.ARTICLE.FETCH_ARTICLE_DETAILS,
  });
}

function setArticleDetails(data: Post) {
  return {
    type: types.ARTICLE.SET_ARTICLE_DETAILS,
    payload: data,
  };
}

function apiAction({
  url = '',
  method = 'GET',
  data = null,
  accessToken = null,
  onSuccess = () => {},
  onFailure = () => {},
  label = '',
  headersOverride = null,
}) {
  return {
    type: API,
    payload: {
      url,
      method,
      data,
      accessToken,
      onSuccess,
      onFailure,
      label,
      headersOverride,
    },
  };
}
