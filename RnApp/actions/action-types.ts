function createActionTypes(base: string, types: string[]) {
  const res: {[s: string]: string} = {};
  types.forEach(type => (res[type] = `${base}/${type}`));
  return res;
}

export const API = 'API';
export const COUNTER = createActionTypes('COUNTER', ['INCREMENT', 'DECREMENT']);
export const ARTICLE = createActionTypes('ARTICLE', [
  'FETCH_ARTICLE_DETAILS',
  'SET_ARTICLE_DETAILS',
]);
