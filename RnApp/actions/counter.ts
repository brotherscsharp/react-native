import {action} from 'typesafe-actions';
import * as types from './action-types';

export const incrementNumber = () => action(types.COUNTER.INCREMENT, undefined);

export const decrementNumber = () => action(types.COUNTER.DECREMENT, undefined);
