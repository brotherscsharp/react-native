import {createSlice, createAsyncThunk, unwrapResult} from '@reduxjs/toolkit';
import {RemoteService} from '../networking/RemoteService';
import {Post} from '../models/Post';

export const INITIAL_STATE = {
  data: undefined,
};

export const getPosts = createAsyncThunk(
  'postsSlice/getPosts',
  async (payload: {}, {getState}) => {
    try {
      const state = getState() as any;
      const res = await RemoteService.getPosts();
      return {
        data: res.data as Post,
      };
    } catch (error) {
      return {error: 'error'};
    }
  },
);

const postsSlice = createSlice({
  name: 'postsSlice',
  initialState: INITIAL_STATE,
  reducers: {},
  extraReducers: {
    // @ts-ignore
    [getPosts.fulfilled]: (state, action) => {
      const response = unwrapResult(action);
      state.data = response.data;
      console.log(`========== state  ${JSON.stringify(state)} `);
    },
  },
});

export const {} = postsSlice.actions;

export default postsSlice.reducer;
