import * as types from '../actions/action-types';

export default function (state = {}, action) {
  switch (action.type) {
    case types.ARTICLE.SET_ARTICLE_DETAILS:
      return {
        ...state,
        data: action.payload,
      };
    default:
      return state;
  }
}
