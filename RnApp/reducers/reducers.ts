import counter from './counter';
import article from './article';
import {combineReducers} from 'redux';

const allReducers = combineReducers({
  counter,
  article,
});
export default allReducers;
