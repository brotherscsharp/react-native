'use strict';
import {COUNTER} from '../actions/action-types';
const initState = {
  value: 0,
};

export default function counterReducer(state = initState, action: any = {}) {
  switch (action.type) {
    case COUNTER.INCREMENT:
      return {...state, value: state.value + 1};
    case COUNTER.DECREMENT:
      return {...state, value: state.value - 1};
    default:
      return state;
  }
}
