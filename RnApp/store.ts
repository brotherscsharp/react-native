import {configureStore} from '@reduxjs/toolkit';
import counterReducer from './features/counter/counterSlice';
import articleReducer from './features/articleSlice';

export default configureStore({
  reducer: {
    counter: counterReducer,
    article: articleReducer,
  },
});
