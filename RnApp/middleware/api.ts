// inspired by https://leanpub.com/redux-book
import axios from 'axios';
import {API} from '../actions/action-types';

const apiMiddleware =
  ({dispatch}) =>
  next =>
  action => {
    next(action);

    if (action.type !== API) return;

    const {url, method, data, onSuccess, onFailure} = action.payload;

    // axios default configs
    axios.defaults.baseURL = process.env.REACT_APP_BASE_URL || '';
    axios.defaults.headers.common['Content-Type'] = 'application/json';
    axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

    axios.get(url).then(({data}) => {
      dispatch(onSuccess(data));
    });
  };

export default apiMiddleware;
