import React, {useEffect} from 'react';
import {Button, View} from 'react-native';
import Article from './controls/Article';
import {connect, useDispatch, useSelector} from 'react-redux';
import {fetchArticleDetails} from '../actions/article';
import {getPosts} from '../features/articleSlice';
import {any} from 'prop-types';

interface Props {
  data: any;
  getPosts1: () => void;
}
const ArticleScreen = ({data, getPosts1}: Props) => {
  const dispatch = useDispatch();

  useEffect(() => {
    //dispatch(fetchArticleDetails());
    getPosts1();
  }, []);

  //const data = useSelector(state => state.article.data);

  const {title, subtitle, paragraphs} = data || {
    title: '',
    subtitle: '',
    paragraphs: [],
  };

  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Article title={title} subtitle={subtitle} paragraphs={paragraphs} />
    </View>
  );
};

const mapStateToProps = (state: any) => {
  return {
    data: state.article.data,
  };
};

const mapDispatchToProps = (dispatch: any) => ({
  getPosts1: () => dispatch(getPosts({})),
});

//export default ArticleScreen;
export default connect(mapStateToProps, mapDispatchToProps)(ArticleScreen);
