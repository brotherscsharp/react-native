import React from 'react';
import {Button, Text, View} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';

// redux toolkit
import {decrement, increment} from '../features/counter/counterSlice';

// redux
import {incrementNumber, decrementNumber} from '../actions/counter';

const CounterScreen = () => {
  const count = useSelector(state => state.counter.value);

  // redux
  const counter = useSelector(state => state.counter.value);

  const dispatch = useDispatch();

  return (
    <View>
      <View>
        <Text>Redux toolkit</Text>
        <Button title="+" onPress={() => dispatch(increment())}>
          Increment
        </Button>

        <Text style={{textAlign: 'center'}}>{count}</Text>
        <Button title="-" onPress={() => dispatch(decrement())}>
          Decrement
        </Button>
      </View>

      <View>
        <Text>Redux</Text>
        <Button title="+" onPress={() => dispatch(incrementNumber())}>
          Increment
        </Button>

        <Text style={{textAlign: 'center'}}>{counter}</Text>
        <Button title="-" onPress={() => dispatch(decrementNumber())}>
          Decrement
        </Button>
      </View>
    </View>
  );
};

export default CounterScreen;
