import React, {useEffect, useState} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import {RemoteService} from '../networking/RemoteService';
import FastImage from 'react-native-fast-image'

const FlatListScreen = () => {
  const [items, setItems] = useState([]);
  const keyExtractor = (item, index) => index;
  const renderItem = ({item}) => (
    <View style={styles.item}>
      <FastImage
        style={{ width: 200, height: 200 }}
        source={{
            uri: item.image,
            priority: FastImage.priority.normal,
        }}
        resizeMode={FastImage.resizeMode.contain}
      />
      <Text style={styles.title}>{item.text}</Text>
    </View>
  );

  useEffect(() => {
    (async () => {
      let response = await RemoteService.getJson();
      setItems(response.data);
    })();
  }, []);

  return (
    <View>
      <Text
        style={{
          fontSize: 32,
        }}>
        FlatList test page
      </Text>
      <FlatList
      
        style={styles.container}
        data={items}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        getItemLayout={(data, index) => (
          {length: 300, offset: 300 * index, index}
        )}
      />
    </View>
  );
};

export default FlatListScreen;

const styles = StyleSheet.create({
  container: {},
  item: {
    backgroundColor: 'olive',
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 25,
  },
});
