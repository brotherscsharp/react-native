import React, { useState } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";

let VeryExpensive: any = null;

const RamScreen = () => {
    const [needsExpensive, setNeedsExpensive] = useState(false);

  const didPress = () => {
    if (VeryExpensive == null) {
        VeryExpensive = require('./controls/VeryExpensive').default;
    }

    setNeedsExpensive(true);
  };

    return <View>
        <Text style={styles.title}>RAM</Text>
        <TouchableOpacity onPress={didPress}
        
        style={styles.button}>
          <Text style={{alignSelf: 'center'}}>Load</Text>
        </TouchableOpacity>
        {needsExpensive ? <VeryExpensive /> : null}
    </View>
}

export default RamScreen;

const styles = StyleSheet.create({
    title: {
      fontSize: 25,
    },
    button: {
        marginTop: 20,
        padding: 20,
        backgroundColor: '#d8d8d8',

    }
  });