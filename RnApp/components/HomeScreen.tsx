import React from 'react';
import {Button, Text, View} from 'react-native';

const HomeScreen = ({navigation, route}) => {
  React.useEffect(() => {
    if (route.params?.post) {
      // Post updated, do something with `route.params.post`
      // For example, send the post to the server
    }
  }, [route.params?.post]);

  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Home Screen</Text>

      <Button
        title="Go to Details"
        onPress={() =>
          navigation.navigate('Details', {
            itemId: 86,
            otherParam: 'anything you want here',
          })
        }
      />

      <Button title="Counter" onPress={() => navigation.navigate('Counter')} />
      <Button title="Article" onPress={() => navigation.navigate('Article')} />
      <Button
        title="FlatList"
        onPress={() => navigation.navigate('FlatList')}
      />
      <Button
        title="Keyboard avoid"
        onPress={() => navigation.navigate('Keyboard')}
      />

      <Button title="Modal" onPress={() => navigation.navigate('Modal')} />
      <Button title="RAM" onPress={() => navigation.navigate('Ram')} />

      <Button
        title="Create post"
        onPress={() => navigation.navigate('CreatePost')}
      />

      <Text style={{margin: 10}}>Post: {route.params?.post}</Text>
    </View>
  );
};

export default HomeScreen;
