import React from 'react';
import {Text, View} from 'react-native';

const Article = ({title = '', subtitle = '', paragraphs = []}) => {
  return (
    <View style={{flex: 1}}>
      <Text>{title}</Text>
      <Text>{subtitle}</Text>
      {paragraphs.map(paragraph => (
        <Text key={paragraph.text}>{paragraph.text}</Text>
      ))}
    </View>
  );
};

export default Article;
