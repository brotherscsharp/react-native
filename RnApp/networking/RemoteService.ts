import axios from 'axios';

export class RemoteService {
  static getPosts = async () => {
    return await axios.get('https://api.npoint.io/3bb76d4bee766758ad6a');
  };

  static getJson = async () => {
    return await axios.get('https://api.npoint.io/890dafcb8f1b40efaab5');
  };
}
