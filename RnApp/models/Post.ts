export interface Paragraph {
  text: string;
}
export interface Post {
  title: string;
  subtitle: string;
  paragraphs: Paragraph;
  numberOfRecommends: number;
}
