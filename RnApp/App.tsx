/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {
  Button,
  SafeAreaView,
  StyleSheet,
  Text,
  useColorScheme,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {Provider} from 'react-redux';

import {Colors} from 'react-native/Libraries/NewAppScreen';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomeScreen from './components/HomeScreen';
import DetailsScreen from './components/DetailsScreen';
import CreatePostScreen from './components/CreatePostScreen';
import CounterScreen from './components/CounterScreen';
import ArticleScreen from './components/ArticleScreen';
import FlatListScreen from './components/FlatListScreen';
import KeyboardScreen from './components/KeyboardScreen';
import ModalScreen from './components/ModalScreen';
import RamScreen from './components/RamScreen';

import allReducers from './reducers/reducers';
import {applyMiddleware, createStore} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';
import {PersistGate} from 'redux-persist/integration/react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import apiMiddleware from './middleware/api';
import thunkMiddleware from 'redux-thunk';
import store from './store';

const logger = store => next => action => {
  console.log('dispatching', action);
  let result = next(action);
  console.log('next state', store.getState());
  return result;
};

/*  configure store */
const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const persistedReducer = persistReducer(persistConfig, allReducers);
const storePersist = createStore(
  persistedReducer,
  applyMiddleware(thunkMiddleware, logger, apiMiddleware),
);
const persistor = persistStore(storePersist);

//const store1 = createStore(allReducers);
/*  configure store END */

const LogoTitle = ({title}) => {
  return <Text>{title}</Text>;
};

const Stack = createNativeStackNavigator();

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <SafeAreaView
      style={{
        flex: 1,
      }}>
      <NavigationContainer>
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <Stack.Navigator initialRouteName="Home">
              <Stack.Screen
                name="Home"
                component={HomeScreen}
                options={{
                  headerTitle: props => <LogoTitle title={'Overview'} />,
                  headerLeft: () => (
                    <Button
                      onPress={() => console.log('Left button!')}
                      title="Left"
                      color="#000"
                    />
                  ),
                  headerRight: () => (
                    <Button
                      onPress={() => console.log('This is a button!')}
                      title="Info"
                      color="#000"
                    />
                  ),
                }}
              />
              <Stack.Screen name="Details" component={DetailsScreen} />
              <Stack.Screen name="CreatePost" component={CreatePostScreen} />
              <Stack.Screen name="Counter" component={CounterScreen} />
              <Stack.Screen name="FlatList" component={FlatListScreen} />
              <Stack.Screen name="Keyboard" component={KeyboardScreen} />
              <Stack.Screen name="Modal" component={ModalScreen} />
              <Stack.Screen name="Ram" component={RamScreen} />
              
              <Stack.Screen name="Article" component={ArticleScreen} />
            </Stack.Navigator>
          </PersistGate>
        </Provider>
      </NavigationContainer>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
